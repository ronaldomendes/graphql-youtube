interface User {
    id: number;
    name: string;
    email: string;
}

let dataUsers: Array<User> = []
const USER_ADDED: string = 'USER_ADDED'

export default {
    Query: {
        users: () => dataUsers
    },
    Mutation: {
        createUser: (obj, { data }, { pub }) => {
            const newUser = { ...data, id: dataUsers.length + 1 }
            dataUsers.push(newUser)
            pub.publish(USER_ADDED, { userAdded: newUser })
            return newUser
        }
    },
    Subscription: {
        userAdded: {
            subscribe: (obj, args, context) => {
                return context.pub.asyncIterator([USER_ADDED])
            }
        }
    }
}