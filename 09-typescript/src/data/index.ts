export default [
    { id: 1, name: 'Jill Valentine', email: 'missvalentine@email.com' },
    { id: 2, name: 'Chris Redfield', email: 'chrisredfield@email.com' },
    { id: 3, name: 'Claire Redfield', email: 'claireredfield@email.com' },
    { id: 4, name: 'Leon S. Kennedy', email: 'leonkennedy@email.com' }
]