const { gql, ApolloServer } = require('apollo-server')

/**
 * GraphQL Scalar Types
 * Int
 * Float
 * String
 * Boolean
 * ID
 */


/**
 * if you add an exclamation it's required that your field need receiving a specific data type
 * it's also valid for other fields 
 */
const typeDefs = gql`
    type Query {
        age: Int
        salary: Float
        name: String
        active: Boolean!
        id: ID
        tecnologies: [String]!
    }
`

// the resolvers need to have the same contexts of type definitions
const resolvers = {
    Query: {
        age() {
            return 34
        },
        salary() {
            return 9999.99
        },
        name() {
            return 'Ronaldo'
        },
        active() {
            return true
        },
        id() {
            return 'fg346114dfdhs'
        },
        tecnologies() {
            return ['Spring Boot', 'GraphQL', 'Angular']
        }
    }
}

const server = new ApolloServer({ typeDefs, resolvers })

server.listen()
