import { gql } from "@apollo/client";

export const GET_CONTACTS = gql`
query {
    contacts {
        id name email phone
    }
}`;

export const ADD_CONTACT = gql`
mutation createContact($name: String, $email: String, $phone: String) {
  createContact(data: { name: $name email: $email phone: $phone }) {
    id name email phone
  }
}`;

export const DELETE_CONTACT = gql`
mutation deleteContact($id: Int) {
    deleteContact(filter: { id: $id})
}`;

export const UPDATE_CONTACT = gql`
mutation updateContact($id: Int!, $name: String, $email: String, $phone: String) {
  updateContact(id: $id, data: { name: $name email: $email phone: $phone }) {
    id name email phone
  }
}`;