const data = [
    { id: 1, name: 'Jill Valentine', email: 'missvalentine@email.com', phone: '11 987 659 874' },
    { id: 2, name: 'Chris Redfield', email: 'chrisredfield@email.com', phone: '11 987 651 545' },
    { id: 3, name: 'Claire Redfield', email: 'claireredfield@email.com', phone: '11 987 658 045' },
    { id: 4, name: 'Leon S. Kennedy', email: 'leonkennedy@email.com', phone: '11 987 654 321' }
];

export default data;
