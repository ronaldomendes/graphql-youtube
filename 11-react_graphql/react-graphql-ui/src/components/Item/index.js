import React from "react";
import { useContactsContext } from "../../context/ContactsContext";
import { GET_CONTACTS } from "../../graphql";

export default function Item({ item }) {
    const { contacts, form } = useContactsContext();
    return (
        <div className="item">
            <h5 className="item-header">
                <a href="name" className="item-title" onClick={e => {
                    e.preventDefault()
                    form.handleUpdate(item)
                }}>
                    {item.name}
                </a>
                <button type="button" className="close" onClick={
                    () => contacts.deleteContact({
                        variables: { id: item.id },
                        refetchQueries: [{ query: GET_CONTACTS }]
                    })}>
                    <span>&times;</span>
                </button>
            </h5>
            <div className="item-body">
                <p className="item-text">{item.email}</p>
                <p className="item-text">{item.phone}</p>
            </div>
        </div>
    );
}