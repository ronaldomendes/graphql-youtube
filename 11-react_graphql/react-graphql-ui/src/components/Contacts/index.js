import React from 'react';
import { useContactsContext } from '../../context/ContactsContext';
import Item from '../Item';

const ContactsContainer = ({ children }) => <div className="contacts">{children}</div>;

export default function Contacts() {
    const { contacts } = useContactsContext()
    if (contacts.loading) return <ContactsContainer>Loading...</ContactsContainer>;

    return (
        <ContactsContainer>
            {contacts.itens.map((item, index) => (
                < Item key={index} item={item} />
            ))}
        </ContactsContainer>
    );
}
