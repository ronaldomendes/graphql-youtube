import React from 'react';
import { useContactsContext } from '../../context/ContactsContext';

export default function Form() {
    const { form } = useContactsContext();

    return (
        <form onSubmit={form.handleSubmit}>
            <input type="hidden" name="id" ref={form.refId} />
            <div className="form-group">
                <label>Name</label>
                <input type="text" name="name" ref={form.refName} />
            </div>
            <div className="form-group">
                <label>E-mail</label>
                <input type="email" name="email" ref={form.refEmail} />
            </div>
            <div className="form-group">
                <label>Phone</label>
                <input type="text" name="phone" ref={form.refPhone} />
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-primary btn-block">Add New</button>
            </div>
        </form>
    );
}