import React from 'react';

const header = ({ text }) => (
    <div className="container-title">
        <h1 className="title">{text}</h1>
    </div>
);

export default header;