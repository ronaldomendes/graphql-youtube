import { useMutation, useQuery } from "@apollo/client";
import React, { useContext } from "react";
import { ADD_CONTACT, DELETE_CONTACT, GET_CONTACTS, UPDATE_CONTACT } from "../graphql";

const Context = React.createContext();

const retrievingAllData = {
    update(cache, { data }) {
        const newContact = data?.createContact;
        const cachedContacts = cache.readQuery({ query: GET_CONTACTS });

        cache.writeQuery({
            query: GET_CONTACTS,
            data: { contacts: [...cachedContacts.contacts, newContact] }
        });
    }
};

export default function ContactsContextProvider({ children }) {
    const { loading, data } = useQuery(GET_CONTACTS);
    const [createContact] = useMutation(ADD_CONTACT, retrievingAllData);
    const [deleteContact] = useMutation(DELETE_CONTACT);
    const [updateContact] = useMutation(UPDATE_CONTACT);

    const [refId, refName, refEmail, refPhone] = customRef(4);

    function handleSubmit(event) {
        event.preventDefault();

        if (refId.current.value) {
            updateContact({
                variables: {
                    id: parseInt(refId.current.value),
                    name: refName.current.value,
                    email: refEmail.current.value,
                    phone: refPhone.current.value

                }
            })
            refId.current.value = '';
        } else {
            createContact({
                variables: {
                    name: refName.current.value,
                    email: refEmail.current.value,
                    phone: refPhone.current.value
                }
            });
        }

        refName.current.value = '';
        refEmail.current.value = '';
        refPhone.current.value = '';
    }

    function handleUpdate(item) {
        refId.current.value = item.id;
        refName.current.value = item.name;
        refEmail.current.value = item.email;
        refPhone.current.value = item.phone;
    }

    return <Context.Provider value={{
        contacts: {
            itens: data ? data.contacts : [],
            loading,
            deleteContact
        },
        form: {
            handleSubmit,
            handleUpdate,
            refId,
            refName,
            refEmail,
            refPhone
        }
    }}>
        {children}
    </Context.Provider>
}

function customRef(size) {
    return Array(size).fill(0).map(_ => React.createRef());
}

export function useContactsContext() {
    return useContext(Context);
}