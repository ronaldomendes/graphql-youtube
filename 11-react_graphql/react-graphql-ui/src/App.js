import React from 'react';

import Contacts from './components/Contacts';
import Form from './components/Form';
import Header from './components/Header';

import { client } from './config/client-graphql';
import { ApolloProvider } from '@apollo/client';

import './App.css';
import ContactsContextProvider from './context/ContactsContext';

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="container">
        <Header text="Contacts" />
        <ContactsContextProvider>
          <main className="main">
            <Form />
            <Contacts />
          </main>
        </ContactsContextProvider>
      </div>
    </ApolloProvider>
  );
}

export default App;
