const { gql, ApolloServer } = require('apollo-server')

/**
 * 
 */

const typeDefs = gql`
    type Product {
        id: ID
        name: String
        value: Float
    }

    type User {
        age: Int
        salary: Float
        name: String
        active: Boolean!
        id: ID
    }

    type Query {
        user: User
        product: [Product]
    }
`

const resolvers = {
    Query: {
        user() {
            return {
                id: 1,
                name: 'Ronaldo',
                age: 34,
                salary: 9999.99,
                active: true
            }
        },
        product() {
            return [{
                id: 1,
                name: 'Smartphone',
                value: 899.99
            }, {
                id: 2,
                name: 'Laptop',
                value: 2999.99
            }]
        }
    }
}

const server = new ApolloServer({ typeDefs, resolvers })

server.listen()
