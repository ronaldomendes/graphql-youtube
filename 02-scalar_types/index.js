const { gql, ApolloServer } = require('apollo-server')

/**
 * GraphQL Scalar Types
 * Int
 * Float
 * String
 * Boolean
 * ID
 */

const typeDefs = gql`
    type Query {
        age: Int
        salary: Float
        name: String
        active: Boolean
        id: ID
    }
`

// the resolvers need to have the same contexts of type definitions
const resolvers = {
    Query: {
        age() {
            return 34
        },
        salary() {
            return 9999.99
        },
        name() {
            return 'Ronaldo'
        },
        active() {
            return true
        },
        id() {
            return 'fg346114dfdhs'
        }
    }
}

const server = new ApolloServer({ typeDefs, resolvers })

server.listen()
