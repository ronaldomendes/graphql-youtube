const db = require('../../../db')

const idGenerator = list => {
    let newId
    let last = list[list.length - 1]
    if (!last) newId = 0
    else newId = last.id
    return ++newId
}

module.exports = {
    User: {
        phone: (obj) => obj.phone || obj.mobile,
        profile: (obj) => db.profiles.find(profile => obj.profile === profile.id)
    },
    Query: {
        users: () => db.users,
        // user(obj, args) {
        //     const { id, name } = args
        //     if (id) return db.users.find(user => user.id === id)
        //     return db.users.find(user => user.name === name)
        // },
        user(obj, { filter }) {
            if (filter.id) {
                return db.users.find(user => user.id === filter.id)
            } else {
                return db.users.find(user => user.email === filter.email)
            }
        }
    },
    Mutation: {
        createUser(obj, { data, age, salary }) {
            const { email } = data
            const userExists = db.users.some(user => email === user.email)

            if (userExists) {
                throw new Error(`User already exists with e-mail: ${data.email}`)
            }

            const newUser = { ...data, id: idGenerator(db.users), age, salary, profile: 2, active: false }
            db.users.push(newUser)
            return newUser
        },
        updateUser(obj, { id, data }) {
            const user = db.users.find(u => id === u.id)
            const index = db.users.findIndex(u => id === u.id)

            const newUser = { ...user, ...data }
            db.users.splice(index, 1, newUser)
            return newUser
        },
        // deleteUser(obj, { id }) {
        //     const user = db.users.find(u => id === u.id)
        //     db.users = db.users.filter(u => id !== u.id)

        //     return !!user
        // },
        deleteUser(obj, { filter: { id, email } }) {
            return filterDeleteUser(id ? { id } : { email })
        }
    }
}

const filterDeleteUser = (filter) => {
    const key = Object.keys(filter)[0]
    const value = Object.values(filter)[0]

    const user = db.users.find(u => value === u[key])
    db.users = db.users.filter(u => value !== u[key])

    return !!user
}