module.exports = {
    users: [
        {
            id: 1,
            name: 'Jill Valentine',
            email: 'missvalentine@email.com',
            salary: 1234.54,
            active: true,
            age: 23,
            mobile: '11 99999 9999',
            profile: 2
        },
        {
            id: 2,
            name: 'Chris Redfield',
            email: 'chrisredfield@email.com',
            salary: 4321.54,
            active: false,
            age: 30,
            mobile: '11 99999 9999',
            profile: 2
        },
        {
            id: 3,
            name: 'Albert Wesker',
            email: 'albertwesker@email.com',
            salary: 9999.99,
            active: true,
            age: 34,
            phone: '11 98765 4321',
            profile: 1
        }
    ],
    profiles: [
        { id: 1, description: 'ADMIN' },
        { id: 2, description: 'NORMAL' }
    ]
}