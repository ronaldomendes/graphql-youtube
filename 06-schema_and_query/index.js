const { ApolloServer } = require('apollo-server')
const graphql = require('./src/graphql')

const server = new ApolloServer({ ...graphql, 
    formatError: (err) => {
        if (err.message.startsWith('User already exists')) {
            return new Error(err.message)            
        }
        return err.message
    }
})

server.listen().then(({ url }) => console.log(url))
