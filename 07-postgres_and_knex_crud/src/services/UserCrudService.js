const db = require('../db')

class UserCrudService {
    constructor(service) {
        this.service = service
    }

    getAllContacts = async () => await this.service('contacts')
    createContact = async (data) => await (await this.service('contacts').insert(data).returning('*'))[0]
    updateContact = async (id, data) => await (await this.service('contacts').where({ id }).update(data).returning('*'))[0]
    deleteContact = async (filter) => {
        if (filter.id) {
            return await this.service('contacts').where({ id: filter.id }).delete()
        } else if (filter.email) {
            return await this.service('contacts').where({ email: filter.email }).delete()
        } else {
            throw new Error(`Invalid parameter`)
        }
    }
}

module.exports = new UserCrudService(db)
