module.exports = {
    Query: {
        contacts: async (obj, args, context, info) => await context.service.getAllContacts()
    },
    Mutation: {
        createContact: async (obj, { data }, context) => await context.service.createContact(data),
        updateContact: async (obj, { id, data }, context) => await context.service.updateContact(id, data),
        deleteContact: async (obj, { filter }, context) => await context.service.deleteContact(filter)
    }
}
