const { ApolloServer } = require('apollo-server')
const graphql = require('./src/graphql')
const UserCrudService = require('./src/services/UserCrudService')

const server = new ApolloServer({
    ...graphql,
    formatError: (err) => err.message,
    context: () => ({ service: UserCrudService })
})

server.listen().then(({ url }) => console.log(url))
