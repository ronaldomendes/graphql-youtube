const { gql, ApolloServer } = require('apollo-server')

const db = {
    products: [
        {
            id: 1,
            name: 'Notebook',
            value: 12000.32
        },
        {
            id: 2,
            name: 'TV',
            value: 6000.32
        },
        {
            id: 3,
            name: 'Monitor',
            value: 2000.0
        }
    ],
    users: [
        {
            id: 1,
            name: 'Paulo',
            salary: 1234.54,
            active: true,
            age: 23,
            mobile: '11 99999 9999',
            profile: 2
        },
        {
            id: 2,
            name: 'Lucas',
            salary: 4321.54,
            active: false,
            age: 30,
            mobile: '11 99999 9999',
            profile: 2
        },
        {
            id: 3,
            name: 'Ronaldo',
            salary: 9999.99,
            active: true,
            age: 34,
            phone: '11 98765 4321',
            profile: 1
        }
    ],
    profiles: [
        { id: 1, description: 'ADMIN' },
        { id: 2, description: 'NORMAL' }
    ]
}

const typeDefs = gql`
    enum ProfileOption {
        ADMIN
        NORMAL
    }

    type Product {
        id: ID
        name: String
        value: Float
    }

    type User {
        age: Int
        salary: Float
        name: String
        active: Boolean!
        id: ID
        phone: String
        profile: Profile
    }

    type Profile {
        id: Int
        description: ProfileOption
    }

    type Query {
        users: [User]
        products: [Product]
        user(id: Int, name: String): User
        product(id: Int): Product
        profiles: [Profile]
    }
`

const resolvers = {
    User: {
        phone: (obj) => obj.phone || obj.mobile,
        profile: (obj) => db.profiles.find(profile => obj.profile === profile.id)
    },
    Query: {
        users: () => users,
        user(obj, args) {
            const { id, name } = args
            if (id) return db.users.find(user => user.id === id)
            return db.users.find(user => user.name === name)
        },
        products: () => db.products,
        product(obj, args) {
            return db.products.find(product => product.id === args.id)
        },
        profiles: () => db.profiles
    }
}

const server = new ApolloServer({ typeDefs, resolvers })

server.listen()
