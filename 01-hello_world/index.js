const { gql, ApolloServer } = require('apollo-server')


const resolvers = {
    Query: {
        hello() {
            return 'Hello World';
        }
    }
}
const typeDefs = gql`
    type Query {
        hello: String
    }
    `
const server = new ApolloServer({ typeDefs, resolvers })

// by default ApolloServer will run on port 4000
server.listen()