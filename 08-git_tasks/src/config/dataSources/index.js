const GitHubService = require("../../services/GitHubService");
const RegistrationService = require("../../services/RegistrationService");
const TaskService = require("../../services/TaskService");

module.exports = () => ({
    gitHubService: GitHubService,
    registrationService: RegistrationService,
    taskService: TaskService
})
