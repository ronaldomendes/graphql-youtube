const generator = require("../../helpers/generator")
const PermissionDeniedError = require('../../exceptions/PermissionDeniedError')

module.exports = ({ req }) => {
    const token = req.headers.authorization
    return {
        validate() {
            try {
                const { id } = generator.verifyToken(token)
                return id
            } catch (err) {
                throw new PermissionDeniedError('Authentication denied')
            }
        }
    }
}
