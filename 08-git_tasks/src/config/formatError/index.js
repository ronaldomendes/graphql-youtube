const PermissionDeniedError = require("../../exceptions/PermissionDeniedError")
const TaskNotFoundError = require("../../exceptions/TaskNotFoundError")
const UserNotFoundError = require("../../exceptions/UserNotFoundError")

module.exports = (err) => {
    if (err.originalError instanceof PermissionDeniedError) {
        return new Error(err.message)
    }
    if (err.originalError instanceof TaskNotFoundError) {
        return new Error(err.message)
    }
    if (err.originalError instanceof UserNotFoundError) {
        return new Error(err.message)
    }
    return err.message
}
