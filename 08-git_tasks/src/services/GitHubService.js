const { RESTDataSource } = require("apollo-datasource-rest")
const UserNotFoundError = require('../exceptions/UserNotFoundError')


class GitHubService extends RESTDataSource {
    constructor() {
        super()
        this.baseURL = 'https://api.github.com'
    }

    async getUser(login) {
        try {
            return await this.get(`/users/${login}`)
        } catch (err) {
            if (err.extensions.response.status === 404) {
                throw new UserNotFoundError(`User not found: ${login}`)                
            }
            throw new Error(err)
        }
    }
}

module.exports = new GitHubService()
