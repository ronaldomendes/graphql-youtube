class PermissionDeniedError extends Error {
    constructor(message, ...args) {
        super(message, ...args)

        this.message = message
        this.name = 'PermissionDeniedError'
    }
}

module.exports = PermissionDeniedError