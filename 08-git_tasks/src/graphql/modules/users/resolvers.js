const generator = require("../../../helpers/generator")

module.exports = {
    User: {
        async tasks(user, obj, { dataSources }) {
            return await dataSources.taskService.getTasks(user.id)
        }
    },
    Query: {
        async user(obj, { login }, { dataSources }) {
            const user = await dataSources.registrationService.getUserByLogin(login)

            if (user) {
                user.token = generator.createToken(user.id)
                return user
            }

            const { login: userLogin, avatar_url } = await dataSources.gitHubService.getUser(login)
            const newUser = await dataSources.registrationService.addUser({
                login: userLogin, avatar_url
            })
            newUser.token = generator.createToken(newUser.id)
            return newUser
        }
    }
}
