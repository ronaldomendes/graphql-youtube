module.exports = {
    Query: {
        async tasks(obj, data, { dataSources, validate }) {
            const user_id = validate()
            return await dataSources.taskService.getTasks(user_id)
        },
        async getTaskById(obj, { id }, { dataSources, validate }) {
            const user_id = validate()
            return await dataSources.taskService.getTaskById(user_id, id)
        }
    },
    Mutation: {
        async addTask(obj, { data }, { dataSources, validate }) {
            const user_id = validate()
            return await dataSources.taskService.addTask(user_id, data)
        },
        async deleteTask(obj, { id }, { dataSources, validate }) {
            const user_id = validate()
            return await dataSources.taskService.removeTask(user_id, id)
        },
        async updateTask(obj, { id, data }, { dataSources, validate }) {
            const user_id = validate()
            return await dataSources.taskService.updateTask(user_id, id, data)
        }
    }
}