const { ApolloServer } = require("apollo-server")
const config = require("./src/config")
const graphql = require("./src/graphql")

const server = new ApolloServer({ ...graphql, ...config })

server.listen().then(({ url }) => console.log(url))
